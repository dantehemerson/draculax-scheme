# DraculaX Scheme for [Sublime Text](http://sublimetext.com)
> A dark theme for [Sublime Text](http://sublimetext.com).

## Dracula Theme Extended with suport for ES6 and React Components

![Screenshot](.github/screenshot.png)

## Install

Go to `Preferences -> Browse Packages`, and then either download and unzip this plugin into that directory, or:

``` bash
git clone https://github.com/dantehemerson/draculax-scheme.git "draculax-scheme"
```


## Author

[![Dante Calderon](https://avatars1.githubusercontent.com/u/18385321?s=96&v=4)](https://github.com/dantehemerson) |
--- |
[Dante Calderon](https://github.com/dantehemerson) |

## License

[MIT License](./LICENSE)
